using System.ComponentModel.DataAnnotations;

namespace Demo.Data.Entities
{

#nullable disable warnings
    public class Item : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string Description { get; set; }

        public virtual User User { get; set; }
    }
}
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable warnings

namespace Demo.Data.Entities
{
    public class User : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        
        public virtual ICollection<Item> Items { get; set; }
    }
}
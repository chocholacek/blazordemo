using Demo.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Demo.Data.Context
{
#nullable disable warnings
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> opts) : base(opts) { }

        public DbSet<User> Users { get; set; }

        public DbSet<Item> Items { get; set; }
    }
}
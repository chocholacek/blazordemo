using Demo.Services.Base;
using Demo.Data.Entities;
using Demo.Data.Context;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Demo.Services
{
    public class UserService : ServiceBase<Demo.Data.Entities.User>
    {
        public UserService(AppDbContext ctx) : base(ctx) { }

        public async Task<IEnumerable<User>> UsersBySubstringAsync(string sub) 
            => await Task.Run(() => Set.Where(u => u.Name.Contains(sub)));

    }
}
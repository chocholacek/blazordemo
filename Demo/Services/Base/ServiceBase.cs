using System.Collections.Generic;
using System.Threading.Tasks;
using Demo.Data.Context;
using Demo.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Demo.Services.Base
{
    public class ServiceBase<TEntity> where TEntity : class, IEntity
    {
        private AppDbContext _db;
        private DbSet<TEntity>? _set;

        protected DbSet<TEntity> Set => _set ??= _db.Set<TEntity>();

        public ServiceBase(AppDbContext db)
        {
            _db = db;
        }

        public async Task CreateAsync(TEntity entity)
        {
            await Set.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task<TEntity> GetAsync(int id) => await Set.FindAsync(id).AsTask();

        public async Task<List<TEntity>> GetAllAsync() => await Set.ToListAsync();

        public async Task UpdateAsync(TEntity entity)
        {
            await Task.Run(() => Set.Update(entity));
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(TEntity entity)
        {
            await Task.Run(() => Set.Remove(entity));
            await _db.SaveChangesAsync();
        }

    }
}